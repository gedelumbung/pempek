#**PEMPEK**

##Just Another My Favorite Indonesian Food

###Quick Setup
* Seetup Vagrant or Docker (prefer Vagrant)
* Clone this repository
* Install Composer (add prestissimo composer plugin is better)
* Install Gulp
* run `composer install`
* run `npm install`
* run `gulp`
* run database migration `php artisan migrate`
* run database seeder `php artisan db:seed`
